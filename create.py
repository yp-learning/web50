# import argparse
import typer
from os import listdir, mkdir
from os.path import isfile, join

# The last day on which project was made is calculated in below line
# it gets the directories in the folder and removes the unwanted items
last_day = max([int(item.split(' ')[1]) for item in listdir('./src/') if not isfile(join('./src/', item)) and ('.' not in item)])

fas_link = '''\n\t<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.14.0/css/font-awesome.min.css" integrity="sha512-1PKOgIY59xJ8Co8+NE6FZ+LOAZKjy+KY8iq0G4B3CyeY6wYHN3yt9PW0XpSriVlkMXe40PTKnXrLnZ9+fkDaog==" crossorigin="anonymous" />'''


def write_file(filename:str, content:str) -> None:
    with open(filename, 'w') as file:
        file.write(content)
    

def html_boilerplate(additionals: str, project_folder:str) -> None:
    """
    Create HTML the HTML Boilerplate
    """
    boilerplate = '<!DOCTYPE html>\n<html lang="en">\n\t<head>\n\t<meta charset="UTF-8">\n\t<meta name="viewport"' \
        f' content="width=device-width, initial-scale=1.0">\n\t<title>Project_title</title>{additionals}\n\t' \
        '<link rel="stylesheet" href="./style.css">\n</head>\n<body>\n\t\n\n\t<script src="script.js"></script>\n</body>\n</html>'

    write_file(f"./src/{project_folder}/index.html", boilerplate)
    

def css_boilerplate(project_folder:str) -> None:
    boilerplate = "@import url('https://fonts.googleapis.com/css2?family=Roboto:wght@400;700&display=swap');\n\n" \
        "* {\n\tbackground-color: #fff;\n\tfont-family: 'Roboto', sans-serif;\n\tdisplay: flex;\n\tflex-direction: column;" \
        "\n\talign-items: center;\n\tjustify-content: center;\n\tmargin: 0;\n\tmin-height: 100vh;\n\toverflow: hidden;\n}"
    
    write_file(f"./src/{project_folder}/style.css", boilerplate)


def js_boilerplate(project_folder:str) -> None:
    write_file(f"./src/{project_folder}/script.js", '')

def main(
    project: str = typer.Argument(..., help="Project Name"), 
    day: int = typer.Option(last_day + 1, "--day", "-d", help="Day of the project"), 
    fas: bool = typer.Option(False, "--fas", "-f", help="Whether to Include font awesome link in boilerplate or not."),
    ) -> None:
    """
    Create.py boilerplate creator for 50 projects 50 days
    """
    project_name = f"Day {day} - {project}"
    typer.echo("Creating Boilerplate code for " + typer.style(project_name, fg=typer.colors.YELLOW))

    try:
        mkdir(join('./src/', project_name))
        typer.echo(typer.style(f"[+] '{project_name}' Folder was created", fg=typer.colors.GREEN))
    except FileExistsError:
        typer.echo(typer.style("[-] Folder already exists, Creating Boilerplate", fg=typer.colors.RED))

    additionals = ''
    if fas: additionals += fas_link
    html_boilerplate(additionals, project_name)
    css_boilerplate(project_name)
    js_boilerplate(project_name)

    typer.echo(f"[+] all files with all the boilerplates included are created")
    typer.echo('Thank you!')


if __name__ == '__main__':
    typer.run(main)
