const jokeElement = document.getElementById('joke')
const jokeBtn = document.getElementById('btn')

const generateJoke = () => {
    fetch('https://icanhazdadjoke.com', {
        headers: {
            'Accept': "application/json"
        }
    }).then(res => res.json()).then(data => jokeElement.innerHTML = data.joke)

}

generateJoke()

jokeBtn.addEventListener('click', generateJoke)
