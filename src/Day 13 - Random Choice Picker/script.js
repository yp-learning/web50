const txtDiv = document.getElementById('tags')
const textarea  = document.getElementById('textarea')

textarea.focus()

textarea.addEventListener('keyup', event => {
    createTags(event.target.value)

    if (event.key === 'Enter'){
        setTimeout(() => {
            event.target.value = ''
        })
        randomSelect()
    }
})


function createTags(input){
    let choices = input.split(',').filter(tag => tag.trim() !== '').map(tag => tag.trim())
    let tags = ''
    choices.forEach(choice => {
        tags += `<span class="tag">${choice}</span>`
    })
    txtDiv.innerHTML = tags
}

function randomSelect() {
    const times = 30
    const interval = setInterval(() => {
        const randomTag = pickRandomTag()
        highlightTag(randomTag)
        setTimeout(() => {
            unhighlightTag(randomTag)
        }, 100);
    }, 100);

    setTimeout(() => {
        clearInterval(interval)
        setTimeout(() => {
            const randomTag = pickRandomTag()
            highlightTag(randomTag)
        }, 100);
    }, times * 100);
}

function pickRandomTag(){
    const tags = document.querySelectorAll('.tag')
    return tags[Math.floor(Math.random() * tags.length)]
}

function highlightTag(tag){
    tag.classList.add('highlight')
}

function unhighlightTag(tag){
    tag.classList.remove('highlight')
}
