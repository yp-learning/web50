// conpletely my solution

const btn_left = document.getElementById('left')
const btn_right = document.getElementById('right')
const slides = document.querySelectorAll('.slide')

slides.forEach(slide => {slide.classList.remove('active')})
slides[0].classList.add('active')

let active_slide = document.querySelector('.active')

changeSlide(slides[0])

btn_left.addEventListener('click', event => {
    if (active_slide.previousElementSibling !== null){
        changeSlide(active_slide.previousElementSibling)
    } else {
        changeSlide(slides[slides.length-1])
    }
})

btn_right.addEventListener('click', event => {
    if (active_slide.nextElementSibling !== null && active_slide.nextElementSibling.classList.contains('slide')){
        changeSlide(active_slide.nextElementSibling)
    } else {
        changeSlide(slides[0])
    }
})

function changeSlide(newActiveSlide){
    active_slide.classList.remove('active')
    active_slide = newActiveSlide
    active_slide.classList.add('active')
    document.body.style.backgroundImage  = active_slide.style.backgroundImage
}
