const toastsContainer = document.getElementById('toasts')
const button = document.getElementById('button')

const messages = [
    'Message One',
    'Message Two',
    'Message Three',
    'Message Four',
    'Message Five'
]

const types = ['info', 'success', 'error']

button.addEventListener('click', () => createNotification())

function createNotification(message=null, type=null) {
    let toast = document.createElement('div')

    toast.classList.add('toast')
    toast.classList.add(type ? type : getRandomType())

    toast.innerHTML = message ? message : getRandomMessage()

    toastsContainer.appendChild(toast)

    setTimeout(() => {
        toast.remove()
    }, 3000);
}

function getRandomMessage(){
    return messages[Math.floor(Math.random() * messages.length)]
}

function getRandomType(){
    return types[Math.floor(Math.random() * types.length)]
}
