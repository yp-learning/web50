const APIURL = 'https://api.github.com/users/'

const main = document.getElementById('main')
const form = document.getElementById('form')
const search = document.getElementById('search')

async function getUser(username) {
    try{
        let { data } = await axios.get(APIURL + username)
        
        createUserCard(data)
        getRepos(username)
    } catch(err){
        if(err.response.status === 404){
            createErrorCard('No user with that username')
        }else {
            createErrorCard(`Error: ${err.response.status}`)
        }
    }
}

function createUserCard(user) {
    const cardHtml = `
    <div class="card">
        <div>
            <img src="${user.avatar_url}" alt='${user.name}' class="avatar"></img>
        </div>
        <div class="user-info">
            <h2>${user.name}</h2>
            <p>${user.bio}</p>

            <ul>
                <li>${user.followers} <strong>Followers</strong></li>
                <li>${user.following} <strong>Following</strong></li>
                <li>${user.public_repos} <strong>Repos</strong></li>
            </ul>

            <div id="repos"></div>
        </div>
    </div>`

    main.innerHTML = cardHtml
}

async function getRepos(username){
    try{
        let { data } = await axios.get(APIURL + username + '/repos?sort=created')
        
        addReposToCard(data)
    } catch(err){
        if(err.response.status){
            createErrorCard(`Error: ${err.response.status}`)
        }
    }
}

function addReposToCard(repos){
    const reposEl = document.getElementById('repos')

    repos.slice(0, 5).forEach(repo => {
        const repolink = document.createElement('a')
        repolink.classList.add('repo')
        repolink.href = repo.html_url
        repolink.target = '_blank'
        repolink.innerText = repo.name

        reposEl.appendChild(repolink)
    })
}

function createErrorCard(msg){
    const cardHtml = `
        <div class="card">
            <h1>${msg}</h1>
        </div>
    `
    main.innerHTML = cardHtml
}

form.addEventListener('submit', event => {
    event.preventDefault()

    const user = search.value
    
    if (user) {
        getUser(user)
        search.value = ''
    }
})
