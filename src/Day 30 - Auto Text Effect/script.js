// bring the needed elements from the DOM
const textEl = document.getElementById('text')
const speedEl = document.getElementById('speed')

// the text which is to be displayed
const text = 'We love Programming'

// variables to control the text
let idx = 1 // do not change manually
let speed = 300 / speedEl.value


// function which writes the text to the DOM
const writeText = () => {
    textEl.innerText = text.slice(0, idx)
    idx++

    // if full text is written reset the text
    if (idx > text.length){
        idx = 1 // you can do idx = 0 and cancel timeout using else block
    }

    setTimeout(writeText, speed)
}

// event handler to handle when the speed is increased or decreased
speedEl.addEventListener('input', () => speed = 300 / speedEl.value)

// start writting the text
writeText()
