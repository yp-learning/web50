// Get the needed elements form the DOM
const image_container = document.getElementById('imgs')
const noOfImages = document.querySelectorAll('img').length
const nextBtn = document.getElementById('right')
const prevBtn = document.getElementById('left')

// to track which image is currently displayed
let imgIdx = 0

// function to change to the next image
function nextImg() {
    imgIdx++
    if (imgIdx > noOfImages - 1){
        imgIdx = 0
    }
    
    changeImage()
}

// function to change to the previous image
function prevImg() {
    imgIdx--
    if (imgIdx < 0){
        imgIdx = noOfImages - 1
    }
    
    changeImage()
}

// the actual function which changes the image
function changeImage(){
    image_container.style.transform = `translateX(${-imgIdx * 500}px)`
}

// automatic image changeing
let interval = setInterval(nextImg, 2000)

// to prevent clash with the auto image change with the button image change
function resetInterval () {
    clearInterval(interval)
    interval = setInterval(nextImg, 2000)
}

// event listners for the manual image change
nextBtn.addEventListener('click', () => {
    nextImg()
    resetInterval()
})

prevBtn.addEventListener('click', () => {
    prevImg()
    resetInterval()
})
