const background = document.getElementById('background')
const passwordInput = document.getElementById('password')

// add the event listner for the input
passwordInput.addEventListener('input',(event) => {
    let password = event.target.value
    let length = password.length
    let blurValue = 20 - length * 2
    background.style.filter = `blur(${blurValue}px)`
} )
