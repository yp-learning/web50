const panel = document.getElementById('panel')
const ratings = document.querySelectorAll('.rating')
const sendBtn = document.getElementById('send')

// fine if we do this for small quantity
// for(let rating of ratings){
//     rating.addEventListener('click', () =>{
//         removeActive()
//         rating.classList.add('active')
//     })
// }

// For a large quantity use event bubbling
const ratingsContainer = document.querySelector('.rating-container')

// this adds the event listner for the container but listnes for all the inner elements
ratingsContainer.addEventListener('click', event => {
    if(event.target.parentNode.classList.contains('rating')){
        removeActive()
        event.target.parentNode.classList.add('active')
    }
})

// display the thank you message after the review is submitted
sendBtn.addEventListener('click', () => {
    let active = document.querySelector('.active')
    if(active != null){
        panel.innerHTML = `
            <i class="fas fa-heart"></i>
            <strong>Thank You!</strong>
            <br>
            <strong>Feedback: ${active.innerText}</strong>
            <p>We'll use your feedback to improve our customer support</p>
        `
    }
})

function removeActive() {
    for(let r of ratings){
        r.classList.remove('active')
    }
}
