const range = document.querySelector('#range')
const label = document.querySelector('label')

setLabelLeft()

// event listner for the input
range.addEventListener('input', event => {
    label.innerHTML = +event.target.value

    setLabelLeft()
})

// function which will change the position of the label
function setLabelLeft(event){
    let value = event ? event.target.value : range.value

    let rangeWidth = getComputedStyle(event ? event.target : range).getPropertyValue('width')
    rangeWidth = +rangeWidth.substring(0, rangeWidth.length - 2)

    let labelWidth = getComputedStyle(label).getPropertyValue('width')
    labelWidth = +labelWidth.substring(0, labelWidth.length - 2)

    const max = event ? event.target.max : range.max
    const min = event ? event.target.min : range.min

    // const left = value * (rangeWidth / max) - labelWidth / 2 + scale(value, min, max, 10, -10)
    const left = value * (rangeWidth / max) + scale(value, min, max, 10, -10)

    label.style.left = `${left}px`
}

// this fucntion can map any numbers to range of outm=Min and outMax
function scale (num, inMin, inMax, outMin, outMax) {
    return (num -inMin) * (outMax - outMin) / (inMax - inMin) + outMin
}