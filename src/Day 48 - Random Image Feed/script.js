// DOM elements
const container = document.querySelector('.container')

// unsplash url for getting random images
const unsplashURL = 'https://source.unsplash.com/random/'

// number of row you want 
let rows = 5

// creating the elements
for(let i = 0; i < rows * 3; i++){
    const img = document.createElement('img')
    // get the image using the url and the random size
    img.src = `${unsplashURL}${getRandomSize()}`

    // put it into the DOM
    container.appendChild(img)
}

// function to get the random size for the image
function getRandomSize() {
    return `${getRandomNumber()}x${getRandomNumber()}`
}

// funciton ot get a random number between 301 ~ 310
function getRandomNumber() {
    return Math.floor(Math.random() * 10 + 300)
}


