// Needed DOM elements
const screens = document.querySelectorAll('.screen')
const startBtn = document.getElementById('start-btn')
const chooseInsectBtns = document.querySelectorAll('.choose-insect-btn')
const gameContainer = document.getElementById('game-container')
const timeEl = document.getElementById('time')
const scoreEl = document.getElementById('score')
const messageEl = document.getElementById('message')

// Needed global variables
let seconds = 0
let score = 0
let selectedInsect = {}

// event listner for starting the game
startBtn.addEventListener('click', () => screens[0].classList.add('up'))

// event listner to select the insect and start the game
chooseInsectBtns.forEach(btn => {
    btn.addEventListener('click', () => {
        const img = btn.querySelector('img')
        const src = img.getAttribute('src')
        const alt = img.getAttribute('alt')

        selectedInsect = { src, alt }
        
        screens[1].classList.add('up')
        
        setTimeout(createInsect, 1000)
        startGame()
    })
})

// start the timer
function startGame() {
    setInterval(increaseTime, 1000);
}

// timer formating
function increaseTime(){
    let s = String(Math.floor(seconds / 60))
    let m = String(seconds % 60)

    timeEl.innerHTML = `Time: ${s.padStart(2, 0)}:${m.padStart(2, 0)}`
    
    seconds++
}

// creating and adding insect to the DOM
function createInsect() {
    const insect = document.createElement('div')
    insect.classList.add('insect')

    const {x, y} = getRandomLocation()

    insect.style.top = `${x}px`
    insect.style.left = `${y}px`

    insect.innerHTML = `<img src="${selectedInsect.src}" alt="${selectedInsect.alt}" style="transform: rotate(${Math.random() * 360}deg)" />`

    insect.addEventListener('click', catchInsect)

    gameContainer.appendChild(insect)
}

// get a random location for the insect
function getRandomLocation() {
    const width = window.innerWidth
    const height = window.innerHeight
    
    const x = Math.random() * (width - 200) + 100
    const y = Math.random() * (height - 200) + 100
    
    return { x, y }
}

// what happens if the insect is caught
function catchInsect() {
    increaseScore()
    this.classList.add('caught')
    setTimeout(() => this.remove(), 2000)
    addInsects()
}

// add two insects for each one caught
function addInsects() {
    setTimeout(createInsect, 1000)
    setTimeout(createInsect, 2000)
}

// function to update the score board
function increaseScore() {
    score++
    if(score > 19 && !messageEl.classList.contains('visible')){
        messageEl.classList.add('visible')
    }

    scoreEl.innerHTML = `Score: ${score}`
}
