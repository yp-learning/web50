const lables = document.querySelectorAll('.form-control label')

lables.forEach(label => {
    label.innerHTML = label.innerText.split('').map((letter, index) => `<span style="transition-delay:${(index+1)*50}ms">${letter}</span>`).join('')
})